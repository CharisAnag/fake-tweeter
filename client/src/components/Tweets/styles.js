import { makeStyles } from "@material-ui/core";
import { grey, white } from "@material-ui/core/colors";

export default makeStyles(() => ({
  list: {
    backgroundColor: "#e7ebef",
    padding: 20,
  },
  listItem: {
    backgroundColor: grey[50],
    margin: "10px 0",
  },
}));
