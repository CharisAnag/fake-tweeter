import React from "react";
import { List, ListItem, CircularProgress, Paper } from "@material-ui/core";
import { useSelector } from "react-redux";
import Tweet from "./Tweet/Tweet";
import useStyles from "./styles";

const Tweets = (props) => {
  const tweets = useSelector((state) => state.tweets);
  const classes = useStyles();

  const { onCurrentTweet } = props;

  const handleItemClick = (value) => {
    onCurrentTweet(value);
  };

  return !tweets.length ? (
    <CircularProgress />
  ) : (
    <>
      <Paper style={{ maxHeight: 600, overflow: "auto" }}>
        <List className={classes.list} sx={{ width: "100%", maxHeight: "300" }}>
          {tweets.map((tweet) => (
            <ListItem
              button
              className={classes.listItem}
              key={tweet._id}
              onClick={() => handleItemClick(tweet)}
            >
              <Tweet tweet={tweet} />
            </ListItem>
          ))}
        </List>
      </Paper>
    </>
  );
};

export default Tweets;
