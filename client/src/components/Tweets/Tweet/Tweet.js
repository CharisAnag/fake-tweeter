import React from "react";

const Tweet = ({ tweet }) => {
  return <h3>{tweet.message}</h3>;
};
export default Tweet;
