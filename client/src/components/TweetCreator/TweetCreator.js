import React, { useState } from "react";
import {
  Typography,
  TextField,
  Button,
  Paper,
  TextareaAutosize,
  Box,
} from "@material-ui/core";
import { useDispatch } from "react-redux";
import useStyles from "./styles";
import { saveTweet } from "../../actions/tweets";

const TweetCreator = () => {
  const [tweeterData, setTweeterData] = useState({
    message: "",
  });
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch(saveTweet(tweeterData));
    setTweeterData({
      message: "",
    });
  };

  return (
    <Paper>
      <form autoComplete="off" onSubmit={handleSubmit}>
        <Typography variant="h6">
          <Box sx={{ fontWeight: "bold", ml: 2 }}>Fake Tweeter</Box>
        </Typography>
        <Box sx={{ pl: 2 }}>
          <TextField
            label="What is Happening?"
            variant="standard"
            InputProps={{
              disableUnderline: true,
            }}
            multiline
            fullWidth
            value={tweeterData.message}
            onChange={(e) =>
              setTweeterData({ ...tweeterData, message: e.target.value })
            }
          />
        </Box>

        <Box textAlign="center" sx={{ py: 3 }}>
          <Button
            className={classes.rounded}
            variant="contained"
            color="primary"
            size="large"
            type="submit"
            alignitems="center"
            justify="center"
            disabled={!tweeterData.message}
          >
            Submit
          </Button>
        </Box>
      </form>
    </Paper>
  );
};
export default TweetCreator;
