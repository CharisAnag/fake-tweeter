import { makeStyles } from "@material-ui/core";

export default makeStyles(() => ({
  noBorder: {
    border: "none",
  },
  rounded: {
    borderRadius: 30,
  },
}));
