import { TextField } from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import React from "react";
import { useSelector } from "react-redux";
import useStyles from "./styles";

const SearchTweets = (props) => {
  const { onCurrentTweet } = props;
  const tweets = useSelector((state) => state.tweets);
  const classes = useStyles();
  const handleChange = (tweet) => {
    if (tweet) {
      onCurrentTweet(tweet);
    }
  };
  return (
    <Autocomplete
      freeSolo
      options={tweets}
      getOptionLabel={(tweet) => tweet.message}
      style={{ margin: "0 auto", maxWidth: 600 }}
      renderInput={(params) => (
        <TextField {...params} label="Search tweet" variant="outlined" />
      )}
      onChange={(e, value) => handleChange(value)}
    />
  );
};
export default SearchTweets;
