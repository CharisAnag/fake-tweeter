import { Box, Typography, Grid } from "@material-ui/core";
import React from "react";
import useStyles from "./styles";

const ShowTweet = ({ results }) => {
  const classes = useStyles();

  return (
    <Grid
      container
      alignItems="center"
      justifyContent="center"
      spacing={0}
      style={{ minHeight: "50vh" }}
    >
      <Grid item xs={8}>
        <Box
          className={classes.boxStyle}
          sx={{
            alignContent: "center",
            justifyContent: "center",
            display: "flex",
            alignItems: "center",
            p: 1,
          }}
        >
          <Typography variant="h6">{results.message ?? " "}</Typography>
        </Box>
      </Grid>
    </Grid>
  );
};
export default ShowTweet;
