import { makeStyles } from "@material-ui/core";
import { grey, blue } from "@material-ui/core/colors";

export default makeStyles(() => ({
  boxStyle: {
    backgroundColor: "#f7f9f9",
    borderRadius: 20,
    height: 300,
  },
}));
