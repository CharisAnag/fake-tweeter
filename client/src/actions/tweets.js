import * as api from "../api";

export const getTweets = () => async (dispatch) => {
  try {
    const { data } = await api.getTweets();
    dispatch({ type: "FETCH_ALL", payload: data });
  } catch (err) {
    console.log(err.message);
  }
};

export const saveTweet = (tweet) => async (dispatch) => {
  try {
    const { data } = await api.saveTweet(tweet);
    dispatch({ type: "SAVE", payload: data });
  } catch (err) {
    console.log(err);
  }
};
