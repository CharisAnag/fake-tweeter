import React, { useEffect, useState } from "react";
import { Container, Divider, Typography, Grow, Grid } from "@material-ui/core";
import Tweets from "./components/Tweets/Tweets";
import TweetCreator from "./components/TweetCreator/TweetCreator";
import SearchTweets from "./components/SearchTweets/SearchTweets";
import ShowTweet from "./components/ShowTweet/showTweet";
import useStyles from "./styles";
import { useDispatch } from "react-redux";

import { getTweets } from "./actions/tweets";

const App = () => {
  const [currentTweeet, setCurrentTweet] = useState({});
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getTweets());
  }, [dispatch]);

  const onCurrentTweet = (tweet) => {
    setCurrentTweet(tweet);
  };
  return (
    <Container maxWidth="lg">
      <Grow in>
        <Grid container>
          <Grid item xs={6} sm={6}>
            <TweetCreator />
            <Tweets onCurrentTweet={onCurrentTweet} />
          </Grid>
          <Grid item xs={6} sm={6}>
            <SearchTweets onCurrentTweet={onCurrentTweet} />
            <ShowTweet results={currentTweeet} />
          </Grid>
        </Grid>
      </Grow>
    </Container>
  );
};

export default App;
