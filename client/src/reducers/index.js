import { combineReducers } from "redux";
import tweets from "./tweets";

export const reducers = combineReducers({ tweets });
