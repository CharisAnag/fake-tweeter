export default (tweets = [], action) => {
  switch (action.type) {
    case "FETCH_ALL":
      return action.payload;
    case "SAVE":
      return [action.payload, ...tweets];
    default:
      return tweets;
  }
};
