import axios from "axios";

const url = "http://localhost:8000/tweets";

export const getTweets = () => axios.get(url);
export const saveTweet = (newTweet) => axios.post(url, newTweet);
