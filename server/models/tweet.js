import mongoose from "mongoose";

const tweetSchema = mongoose.Schema(
  {
    message: { type: String, require: true },
    // created: {
    //   type: Date,
    //   default: new Date(),
    // },
  },
  { timestamps: true }
);

const Tweet = mongoose.model("Tweet", tweetSchema);

export default Tweet;
