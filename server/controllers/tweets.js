import express from "express";
import mongoose from "mongoose";

import Tweet from "../models/tweet.js";

export const getTweets = async (req, res) => {
  try {
    const tweets = await Tweet.find().sort({ createdAt: -1 });

    res.status(200).json(tweets);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

export const saveTweet = async (req, res) => {
  const { message } = req.body;

  const newTweet = new Tweet({ message });
  console.log(newTweet);

  try {
    await newTweet.save();

    res.status(201).json(newTweet);
  } catch (err) {
    res.status(409).json({ message: error.message });
  }
};
