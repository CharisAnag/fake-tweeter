import express from "express";

import { getTweets, saveTweet } from "../controllers/tweets.js";

const router = express.Router();

router.get("/", getTweets);
router.post("/", saveTweet);

export default router;
